![Banner](css/1/portada-web.png)
# Titulo del Proyecto
#### Curriculum Vitae

## Indice

1. [Caracteristicas del proyecto](#Caracteristicas)
2. [Contenido del Proyecto](#contenido-del-proyecto)
3. [Tecnologias que se usaron](#tecnologias-que-se-usaron)
4. [IDE](#IDE)
5. [Instalacion](#Instalacion)
6. [Demo](#Demo)
7. [Autores](#Autores)
8. [Institucion](#Institucion)



#### Caracteristicas
- Uso del CSS recomendado: [ver](https://gitlab.com/leiderMartinez/my-curriculum/-/tree/master/css)
#### Contenido del Proyecto
- [Index](https://quasi201.gitlab.io/primer_cv/)
#### Tecnologias que se usaron
- Uso de Bootstrap: [ir](https://getbootstrap.com)
#### IDE
- Visual Studio Code: [descargar](https://code.visualstudio.com)
#### Instalacion

1. local
    - Descarga el repositorio: [descargar](https://gitlab.com/Quasi201/primer_cv)

#### Demo
![check-code-coverage](https://img.shields.io/badge/HTML-v5.0-orange)
#### Autores
#### Institucion
